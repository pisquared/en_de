# en_de

Generate audio from a dictionary.

## install

```
./INSTALL.sh
```

# run

You first need a service account secret from [Google Cloud Text-To-Speech API](https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries#client-libraries-install-python) (go through the steps of `Before you begin` section, the rest is handled by the app).

Modify what you need from the `generate.py` file:

```
./RUN.sh
```
